# Class Collection represents objects which stores elements of same type together
class Collection
  attr_accessor :array

  # Method to display the odd element in array
  def display_odd_element
    result = find_odd_element(0, array.length - 1)
    puts "The element that appears odd number of times : #{result}"
  end

  private

  # Method to find element with odd no of occurance in Array
  def find_odd_element(start, last)
    return array[start] if start == last

    middle = (start + last) / 2
    if (middle % 2).zero?
      array[middle] == array[middle + 1] ? find_odd_element(middle + 2, last) : find_odd_element(start, middle)
    else
      array[middle] == array[middle - 1] ? find_odd_element(middle + 1, last) : find_odd_element(start, middle - 1)
    end
  end
end

numbers = Collection.new
numbers.array = [5, 5, 2, 2, 5, 5, 2, 2, 25, 5, 5, 50, 50]
puts "Input Array : #{numbers.array}"
numbers.display_odd_element
