require './product'
require './cart'
# Class to manage Mall information
class Mall
  # Adds new Product in Mall
  # @return [void]
  def self.add_product_in_mall
    product = Product.new.accept_details
    if Product.find_product(product.name).nil?
      Product.add(product)
      puts "\nProduct added successfully in Mall"
    else
      puts "\nProduct already exist in Mall"
    end
  end

  # Displays various options to manage mall
  # @return [void]
  def self.display_menu_options
    puts "\n********Welcome to Mall**************"
    puts "1. Add Product in Mall \n2. Add Product in Shopping Cart"
    puts "3. Remove Product from Shopping Cart\n4. Show details of Cart\n5. Quit\n"
    puts '*****************************************'
    puts "\nEnter your Choice :"
  end

  # Manages the mall
  # @return [void]
  def self.manage_mall
    shopping_cart = Cart.new
    loop do
      display_menu_options
      case gets.chomp.to_i
      when 1 then add_product_in_mall
      when 2 then shopping_cart.add_product
      when 3 then shopping_cart.remove_product
      when 4 then shopping_cart.display_details
      when 5
        shopping_cart.display_details('checkout')
        break
      else puts 'Invalid Input'
      end
    end
  end
end
Mall.manage_mall
