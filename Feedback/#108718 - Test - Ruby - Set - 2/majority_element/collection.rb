require '../input'
# Class to manage the collection of integers
class Collection
  include Input

  # @return [Array<Integer>] Collection of integers
  attr_reader :array

  # Default Constructor for class collection
  # @return [void]
  def initialize
    @array = []
  end

  # Accepts size and elements of array from user
  # @return [void]
  def accept_input
    @array = Input.accept_array_elements
  end

  # Displays the Majority Element in array if exist
  # @return [void]
  def majority_element
    candidate = find_candidate
    if majority?(candidate)
      puts "The majority Element in given array : #{candidate}"
    else
      puts 'There is no majority element in given array.'
    end
  end

  private

  # Finds the candidate in array eligible to become majority element
  # @return [Integer]
  def find_candidate
    majority_index = 0
    count = 1
    array.length.times do |index|
      array[majority_index] == array[index] ? count += 1 : count -= 1
      if count.zero?
        majority_index = index
        count = 1
      end
    end
    array[majority_index]
  end

  # Checks if the candidate occurs more than n/2 times
  # @param candidate [Integer] Element of array which is needed to verify for majority
  # @return [TrueClass,FalseClass]
  def majority?(candidate)
    count = 0
    array.length.times do |index|
      count += 1 if array[index] == candidate
    end
    count > (array.length / 2)
  end
end

numbers = Collection.new
numbers.accept_input
puts "Input Array :\n#{numbers.array}"
numbers.majority_element
