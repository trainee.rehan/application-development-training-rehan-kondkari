require './input'
# Class Collection represents objects which stores elements of same type together
class Collection
  include Input

  attr_reader :array

  # Default Constructor for class Collection
  def initialize
    @array = []
    @swap_status = true
  end

  # Method to accept size and elements of array from user
  def accept_input
    @array = Input.accept_array_elements
  end

  # Method to sort array using bubble sort
  def bubble_sort
    while @swap_status
      @swap_status = false
      (0...array.length - 1).each do |index|
        if swap?(index, index + 1)
          swap(index, index + 1)
          @swap_status = true
        end
      end
    end
    array
  end

  # Method to sort array using recursive bubble sort
  def recursive_bubble_sort(size = nil)
    size ||= array.length
    return array if size == 1

    (0...size - 1).each do |index|
      swap(index, index + 1) if swap?(index, index + 1)
    end
    recursive_bubble_sort(size - 1)
  end

  private

  # Method to check if swap operation to be performed or not
  def swap?(left_index, right_index)
    array[left_index] > array[right_index]
  end

  # Method to perform swap operation
  def swap(left_index, right_index)
    array[left_index], array[right_index] = array[right_index], array[left_index]
  end
end
numbers = Collection.new
numbers.accept_input
puts "Input Array :\n#{numbers.array}"
puts 'Bubble Sort :'
puts "Sorted Array :\n#{numbers.bubble_sort}"
numbers = Collection.new
numbers.accept_input
puts "Input Array :\n#{numbers.array}"
puts 'Recursive Bubble Sort :'
puts "Sorted Array :\n#{numbers.recursive_bubble_sort}"
