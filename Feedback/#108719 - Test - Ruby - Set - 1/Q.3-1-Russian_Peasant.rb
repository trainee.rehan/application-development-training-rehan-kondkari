# Class FastMultiply represents objects which store operands to be multiply
class FastMultiply
  attr_reader :multiplicand, :multiplier

  # Method to accept values of operands from user
  def accept_operands
    puts 'Enter the value of Multiplicand :'
    @multiplicand = Integer(gets.chomp)
    puts 'Enter the value of Multiplier :'
    @multiplier = Integer(gets.chomp)
  rescue ArgumentError
    puts 'Invalid Input'
  end

  # Method to find product using Russian Peasant Algorithm
  def russian_peasant
    product = 0
    while multiplier.positive?
      product += multiplicand if multiplier.odd?
      @multiplicand *= 2
      @multiplier /= 2
    end
    product
  end
end

operands = FastMultiply.new
operands.accept_operands
puts "Product : #{operands.russian_peasant}"
