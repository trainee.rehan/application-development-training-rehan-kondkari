module Input
  # Method to accept elements of array from user
  def self.accept_array_elements
    array = []
    size = accept_array_size
    puts "\nEnter the elements of Array :"
    size.times { array << accept_element }
    array
  end

  # Method to accept size of array from user
  def self.accept_array_size
    puts "\nEnter the size of Array :"
    size = gets.chomp
    return size.to_i if valid_array_size?(size)

    puts "\nInvalid Input"
    accept_array_size
  end

  # Method to check if the size of array is valid or not
  def self.valid_array_size?(size)
    size.match?(/^\d+?$/) && size.to_i.positive?
  end

  # Method to accept element of array from user
  def self.accept_element
    element = gets.chomp
    return element.to_i if valid_element?(element)

    puts "\nInvalid Array Element, Please re-enter"
    accept_element
  end

  # Method to check if the element of array is valid or not
  def self.valid_element?(element)
    element.match?(/^(?:-)?\d+?$/)
  end
end
