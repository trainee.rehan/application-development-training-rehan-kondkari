require './product'
# Class to manage Cart information
class Cart
  # @return [Array<Product>] Products added in Cart
  attr_reader :products
  # @return [Integer] Sum of the amount of products added in Cart
  attr_reader :total_amount

  # Default Constructor for class Cart
  # @return [void]
  def initialize
    @products = []
    @total_amount = 0
  end

  # Adds the product to cart
  # @return [void]
  def add_product
    product_name = accept_product_name('add')
    product = find_product_in_mall(product_name, 'add')
    return if product.nil?

    product_quantity = accept_product_quantity(product, 'add')
    if find_product(product.name).nil?
      add_new_product(product, product_quantity)
    else
      add_existing_product(product, product_quantity)
    end
    puts "#{product_quantity} #{product.name} has been added in your cart"
  end

  # Removes the product from Cart
  # @return [void]
  def remove_product
    return puts 'Cart is Empty !!' if products.count.zero?

    product_name = accept_product_name('remove')
    product = find_product_in_mall(product_name, 'remove')
    return if product.nil?

    product_in_cart = find_product(product.name)
    return puts "#{product.name} does not exist in Cart" if product_in_cart.nil?

    quantity = accept_product_quantity(product_in_cart, 'remove')
    product_in_cart.quantity == quantity ? products.delete(product_in_cart) : product_in_cart.modify(quantity, 'remove')
    product.modify(quantity, 'add')
    puts "#{quantity} #{product_in_cart.name} has been removed from your cart"
  end

  # Displays the details of Cart
  # @param status [String] Specifies the status of Cart
  # @return [void]
  def display_details(status = nil)
    return puts 'Cart is Empty !!' if products.count.zero? && status != 'checkout'
    return puts "\nTHANKYOU FOR VISTING OUR MALL" if products.count.zero? && status == 'checkout'

    puts "\nShow details of Cart" if status != 'checkout'
    puts "\nTHANKYOU FOR PURCHASING" if status == 'checkout'
    display_invoice
  end

  private

  # Find the product in Cart
  # @param product_name [String] Name of the product
  # @return [Product, NilClass]
  def find_product(product_name)
    products.select { |product_in_cart| product_in_cart.name == product_name }.first
  end

  # Validates the quantity of the Product to be added/removed from Cart
  # @param product [Product]
  # @param quantity [Integer] Quantity of the product
  # @return [TrueClass, FalseClass]
  def valid_quantity?(product, quantity)
    quantity.match?(/^\d+?$/) && quantity.to_i.positive? && quantity.to_i <= product.quantity
  end

  # Accepts the name of product to be added/removed from Cart
  # @param action [String] Specifies the action to be performed on Cart
  # @return [String]
  def accept_product_name(action)
    puts "\nAdd Product in Shopping Cart:" if action == 'add'
    puts "\nRemove Product from Shopping Cart:" if action == 'remove'
    puts 'Enter the name of product :'
    gets.chomp
  end

  # Finds the Product in Mall which is to be added/removed from Cart
  # @param product_name [String] Name of the product
  # @param action [String] Specifies the action to be performed on Cart
  # @return [Product, NilClass]
  def find_product_in_mall(product_name, action)
    product = Product.find_product(product_name)
    return puts "#{product_name} is not available in Mall" if product.nil?

    return puts "#{product_name} is out of stock" if product.quantity.zero? && action == 'add'

    product
  end

  # Accepts the quantity of the Product to be added/removed from Cart
  # @param product [Product]
  # @param action [String] Specifies the action to be performed on Cart
  # @return [Integer]
  def accept_product_quantity(product, action)
    puts "Enter Quantity of #{product.name} to add in Cart :" if action == 'add'
    puts "Enter Quantity of #{product.name} to remove from Cart :" if action == 'remove'
    quantity = gets.chomp
    return quantity.to_i if valid_quantity?(product, quantity)

    puts "Only #{product.quantity} #{product.name} are available in Mall" if action == 'add'
    puts "Only #{product.quantity} #{product.name} are available in Cart" if action == 'remove'
    accept_product_quantity(product, action)
  end

  # Adds more quantity of existing product in Cart
  # @param product [Product]
  # @param quantity [Integer] Quantity of the product
  # @return [void]
  def add_existing_product(product, quantity)
    product_in_cart = find_product(product.name)
    product_in_cart.modify(quantity, 'add')
    product.modify(quantity, 'remove')
  end

  # Adds new Product in Cart
  # @param product [Product]
  # @param quantity [Integer] Quantity of the product
  # @return [void]
  def add_new_product(product, quantity)
    product_in_cart = Product.new
    product_in_cart.name = product.name
    product_in_cart.quantity = quantity
    product_in_cart.unit_price = product.unit_price
    product.modify(quantity, 'remove')
    @products << product_in_cart
  end

  # Display the invoice of products in Cart
  # @return [void]
  def display_invoice
    puts "\nProduct\tQuantity\tPrice/piece\tPrice"
    @total_amount = 0
    products.each do |product|
      product_total_price = product.quantity * product.unit_price
      puts "#{product.name}\t#{product.quantity}\t#{product.unit_price}\t#{product_total_price}"
      @total_amount += product_total_price
    end
    puts "\nTotal Price : #{total_amount}"
  end
end
