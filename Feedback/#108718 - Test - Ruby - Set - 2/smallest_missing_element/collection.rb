require '../input'
# Class to manage the collection of integers
class Collection
  include Input

  # @return [Array<Integer>] Collection of integers
  attr_reader :array

  # Default Constructor for class Collection
  # @return [void]
  def initialize
    @array = []
  end

  # Accepts size and elements of array from user
  # @return [void]
  def accept_input
    @array = Input.accept_array_elements
  end

  # Displays the smallest missing element in array
  # @return [void]
  def display_smallest_missing
    @array.sort!
    start = 0
    last = @array.length - 1
    puts "Smallest Missing Element : #{find_smallest_missing(start, last)}"
  end

  private

  # Finds the smallest missing element in array
  # @param start [Integer] Start index of array
  # @param last [Integer] Last index of array
  # @return [Integer]
  def find_smallest_missing(start, last)
    return last + 1 if start > last
    return start if start != @array[start]

    mid = ((start + last) / 2).to_i
    @array[mid] == mid ? find_smallest_missing(mid + 1, last) : find_smallest_missing(start, mid)
  end
end

numbers = Collection.new
numbers.accept_input
puts "Input Array :\n#{numbers.array}"
numbers.display_smallest_missing
