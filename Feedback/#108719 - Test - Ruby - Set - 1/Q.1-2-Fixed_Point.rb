require './input'
# Class Collection represents object which stores elements of same type together
class Collection
  include Input

  attr_reader :array

  # Default Constructor for class Collection
  def initialize
    @array = []
  end

  # Method to accept size and elements of array from user
  def accept_input
    @array = Input.accept_array_elements
  end

  # Method to find Fixed Point in Sorted Array
  def find_fixed_point
    array.sort!
    puts "Sorted Array : #{array}"
    array.length.times do |index|
      puts "Fixed Point : #{index}" if fixed_point?(index)
    end
  end

  private

  # Method to check if the index is fixed point or not
  def fixed_point?(index)
    index == array[index]
  end
end

numbers = Collection.new
numbers.accept_input
puts "Input Array : #{numbers.array}"
numbers.find_fixed_point
