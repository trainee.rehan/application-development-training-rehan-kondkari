# Class to manage Product information
class Product
  # @return [String] Name of the product
  attr_accessor :name
  # @return [Integer] Per unit price of the product
  attr_accessor :unit_price
  # @return [Integer] Available Quantity of the product
  attr_accessor :quantity

  # Stores all the instance of Product Class
  @@products = []

  # Checks whether the product already exist or not
  # Finds the product by name
  # @param product_name [String] Name of the Product
  # @return [Product, NilClass]
  def self.find_product(product_name)
    @@products.select { |product| product.name.casecmp?(product_name) }.first
  end

  # Modifies the quantity of the product
  # @param product [Product]
  # @return [Array[products]]
  def self.add(product)
    @@products << product
  end

  # Accepts the product details from user
  # @return [void]
  def accept_details
    self.name = accept_name
    self.unit_price = accept_price
    self.quantity = accept_quantity
    self
  end

  # Modifies the quantity of the product
  # @param quantity [Integer] Quantity of product
  # @param action [String] Specifies action to perform on product Quantity
  # @return [void]
  def modify(quantity, action)
    self.quantity += quantity if action == 'add'
    self.quantity -= quantity if action == 'remove'
  end

  private

  # Validates the name of the Product
  # @param product_name [String] Name of the Product
  # @return [TrueClass, FalseClass]
  def valid_name?(product_name)
    product_name.match?(/^\w+(?: \w+)?$/)
  end

  # Validates the price of the Product
  # @param product_price [Integer] Per unit price of the Product
  # @return [TrueClass, FalseClass]
  def valid_price?(product_price)
    product_price.match?(/^\d+?$/) && product_price.to_i.positive?
  end

  # Validates the quantity of the Product
  # @param product_quantity [Integer] Available Quantity of the Product
  # @return [TrueClass, FalseClass]
  def valid_quantity?(product_quantity)
    product_quantity.match?(/^\d+?$/) && product_quantity.to_i.positive?
  end

  # Accepts the name of the Product from user
  # @return [String]
  def accept_name
    puts "\nEnter the name of Product :"
    product_name = gets.chomp
    return product_name if valid_name?(product_name)

    puts "\nInvalid Product Name"
    accept_name
  end

  # Accepts the unit price of the Product from user
  # @return [Integer]
  def accept_price
    puts "\nEnter the per unit of Price of #{name} :"
    product_price = gets.chomp
    return product_price.to_i if valid_price?(product_price)

    puts "\nInvalid Product Price"
    accept_price
  end

  # Accepts the available quantity of the Product from user
  # @return [Integer]
  def accept_quantity
    puts "\nEnter the Quantity of #{name} to add in Mall :"
    product_quantity = gets.chomp
    return product_quantity.to_i if valid_quantity?(product_quantity)

    puts "\nInvalid Product Quantity"
    accept_quantity
  end
end
