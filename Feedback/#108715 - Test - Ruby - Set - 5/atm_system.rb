# #108715 - Test - Ruby - Set - 5 - Q.2 ATM System
require './bank_account'
class AtmSystem
  attr_reader :status

  # Method to display various account activities
  def self.display_account_options
    puts "\n********Welcome to ATM Service**************"
    puts "1. Check Balance\n2. Withdraw Cash\n3. Deposit Cash\n4. Account History\n5. Quit"
    puts '*****************************************'
    puts "\nEnter your choice: "
  end

  # Method to display ATM menu
  def display_menu
    account = login_account
    @status = 'y'
    while status.casecmp?('y')
      select_account_options(account)
      accept_atm_status if status.casecmp?('y')
      break if status.casecmp?('n')

      puts 'Invalid Input' if !status.casecmp?('n') && !status.casecmp?('y')
    end
    puts "\nTHANKYOU FOR USING ATM"
  end

  private

  # Method to login into user bank account
  def login_account
    input_pin = accept_pin
    logged_in_account = BankAccount.accounts.select { |account| account.pin == input_pin }.first
    return logged_in_account unless logged_in_account.nil?

    puts '**THE ENTERED PIN IS INCORRECT**'
    login_account
  end

  # Method to accept secret pin from user
  def accept_pin
    puts 'ENTER YOUR SECRET PIN NUMBER : '
    gets.chomp
  end

  # Method to select various account activities
  def select_account_options(account)
    self.class.display_account_options
    case gets.chomp.to_i
    when 1 then account.display_balance
    when 2 then account.withdraw_amount('Debited through ATM')
    when 3 then account.deposit_amount('Credited through ATM')
    when 4 then account.display_account_history
    when 5 then @status = 'n'
    else puts 'Invalid Input'
    end
  end

  # Method to accept choice from user whether to continue using ATM or not
  def accept_atm_status
    puts "\nDo you want to continue using ATM?(y/n):"
    @status = gets.chomp
  end
end
BankAccount.new('1111', BigDecimal(1000))
BankAccount.new('2222', BigDecimal(2000))
BankAccount.new('3333', BigDecimal(3000))
bank_atm = AtmSystem.new
bank_atm.display_menu
