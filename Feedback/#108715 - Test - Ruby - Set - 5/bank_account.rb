# #108715 - Test - Ruby - Set - 5 - Q.2 ATM System
# Run atm_system.rb to execute Q.2 ATM System
require 'bigdecimal'
class BankAccount
  attr_reader :pin, :balance, :transactions_history

  @@accounts = []

  # Default Constructor for Class BankAccount
  def initialize(pin, amount)
    @transactions_history = []
    @pin = pin
    @balance = amount
    transaction = { credit: amount.to_f, debit: '', note: 'Initial Balance' }
    transactions_history << transaction
    @@accounts << self
  end

  # Methog to get the details of all accounts
  def self.accounts
    @@accounts
  end

  # Method to display balance of BankAccount
  def display_balance
    puts "\nYOUR BALANCE IN Rs: #{balance.to_f}"
  end

  # Method to deposit amount in Bank account
  def deposit_amount(note)
    amount = accept_amount('ENTER THE AMOUNT TO DEPOSIT : ')
    @balance += amount
    transaction = { credit: amount.to_f, debit: '', note: note }
    transactions_history << transaction
    display_balance
  end

  # Method to withdraw amount from Bank Account
  def withdraw_amount(note)
    amount = accept_amount('ENTER THE AMOUNT TO WITHDRAW : ')
    return puts "THERE IS ONLY #{balance.to_f} IN YOUR ACCOUNT" unless amount <= balance

    @balance -= amount
    transaction = { credit: '', debit: amount.to_f, note: note }
    transactions_history << transaction
    puts 'PLEASE COLLECT CASH'
    display_balance
  end

  # Method to display transaction history of Bank Account
  def display_account_history
    puts "\nAccount History"
    puts "\nCredit\tDebit\tNote"
    transactions_history.each do |transaction|
      puts "#{transaction[:credit]}\t#{transaction[:debit]}\t#{transaction[:note]}"
    end
    display_balance
  end

  private

  # Method to accept the amount from user
  def accept_amount(msg)
    puts msg
    amount = gets.chomp
    return BigDecimal(amount) if valid_amount?(amount)

    accept_amount("\nInvalid Amount, Please enter again...")
  end

  # Method to check if the amount is valid or not
  def valid_amount?(amount)
    amount.match?(/^\d+?$/) && BigDecimal(amount).positive?
  end
end
