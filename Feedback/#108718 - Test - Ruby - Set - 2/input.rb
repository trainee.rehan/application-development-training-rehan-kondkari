module Input
  # Accepts elements of array from user
  # @return [Array<Integer>]
  def self.accept_array_elements
    array = []
    size = accept_array_size
    puts "\nEnter the elements of Array :"
    size.times { array << accept_element }
    array
  end

  # Accepts size of array from user
  # @return [Integer]
  def self.accept_array_size
    puts "\nEnter the size of Array :"
    size = gets.chomp
    return size.to_i if valid_array_size?(size)

    puts "\nInvalid Input"
    accept_array_size
  end

  # Checks if the size of array is valid or not
  # @param size [Integer]
  # @return [TrueClass,FalseClass]
  def self.valid_array_size?(size)
    size.match?(/^\d+?$/) && size.to_i.positive?
  end

  # Accepts element of array from user
  # @return [Integer]
  def self.accept_element
    element = gets.chomp
    return element.to_i if valid_element?(element)

    puts "\nInvalid Array Element, Please re-enter"
    accept_element
  end

  # Checks if the element of array is valid or not
  # @param element [Integer] Element of array
  # @return [TrueClass,FalseClass]
  def self.valid_element?(element)
    element.match?(/^(?:-)?\d+?$/)
  end
end
