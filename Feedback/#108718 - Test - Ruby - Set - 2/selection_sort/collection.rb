require '../input'
# Class to manage the collection of integers
class Collection
  include Input

  # @return [Array<Integer>] Collection of integers
  attr_reader :array

  # Default Constructor for class Collection
  # @return [void]
  def initialize
    @array = []
  end

  # Accepts size and elements of array from user
  # @return [void]
  def accept_input
    @array = Input.accept_array_elements
  end

  # Sorts array using selection sort
  # @return [Array<Integer>]
  def selection_sort
    (@array.length - 1).times do |index|
      minimum_index = index
      ((index + 1)...@array.length).each do |next_index|
        minimum_index = next_index if @array[next_index] < @array[minimum_index]
      end
      swap(index, minimum_index) if swap?(index, minimum_index)
    end
    @array
  end

  # Sorts array using recursive selection sort
  # @param index [Integer] Current index of array
  # @return [Array<Integer>]
  def recursive_selection_sort(index = 0)
    return @array if index == @array.length - 1

    minimum_index = find_minimum_index(index, @array.length - 1)
    swap(index, minimum_index) if swap?(index, minimum_index)
    recursive_selection_sort(index + 1)
  end

  private

  # Finds index of smallest value in Array
  # @param index [Integer] Current index of array
  # @param last_index [Integer] Last index of array
  # @return [Integer]
  def find_minimum_index(index, last_index)
    return index if index == last_index

    minimum_index = find_minimum_index(index + 1, last_index)
    @array[index] < @array[minimum_index] ? index : minimum_index
  end

  # Checks whether to perform swap operation or not
  # @param current_index [Integer] Current index of array
  # @param minimum_index [Integer] Index of the smallest element of array
  # @return [TrueClass, FalseClass]
  def swap?(current_index, minimum_index)
    current_index != minimum_index
  end

  # Swaps two elements of array
  # @param current_index [Integer] Current index of array
  # @param minimum_index [Integer] Index of the smallest element of array
  # @return [void]
  def swap(current_index, minimum_index)
    @array[current_index], @array[minimum_index] = @array[minimum_index], @array[current_index]
  end
end

numbers = Collection.new
puts 'Selection Sort :'
numbers.accept_input
puts "Input Array :\n#{numbers.array}"
puts "Sorted Array :\n#{numbers.selection_sort}"
numbers = Collection.new
puts 'Recursive Selection Sort :'
numbers.accept_input
puts "Input Array :\n#{numbers.array}"
puts "Sorted Array :\n#{numbers.recursive_selection_sort}"
