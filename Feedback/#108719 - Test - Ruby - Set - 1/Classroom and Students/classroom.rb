# #108719 - Test - Ruby - Set - 2 - Q.2 Classroom & Students
# Run school.rb to execute Q.2 Classroom & Students
require './student'
# Class to manage Classroom information
class Classroom
  # @return [String] Name of the classroom
  attr_reader :name

  # Stores all the instance of Classroom Class
  @@classrooms = []

  # Finds the classroom by name
  # @param classroom_name [String] Name of the classroom
  # @return [Classroom]
  def self.find_classroom(classroom_name)
    @@classrooms.select { |class_room| class_room.name.casecmp?(classroom_name) }.first
  end

  # Adds a new classroom if not exist
  # @return [void]
  def add_classroom
    @name = accept_name
    if self.class.find_classroom(name).nil?
      @@classrooms << self
      puts "\nClassroom for #{name} has been created"
    else
      puts "\n#{name} Classroom already present"
    end
  end

  # Adds students in classroom
  # @return [void]
  def add_student
    no_of_student = accept_no_of_students
    (1..no_of_student).each do |number|
      puts "\nEnter details of student #{number}"
      Student.new.accept_details(name)
    end
    puts "\n#{no_of_student} Students has been added in #{name}"
  end

  # Displays list of students in Classroom
  # @return [void]
  def display_student_list
    students = Student.find_students_by_classroom(name)
    return puts "\nThis Classroom does not have students" if students.length.zero?

    sorted_students_list = sort_students_list(students)
    puts "Roll Number\tName\tBirthdate\tPercentage"
    sorted_students_list.each do |student|
      puts "#{student.roll_no}\t#{student.name}\t#{student.birth_date.strftime('%-d %B %Y')}\t#{student.percentage}"
    end
  end

  private

  # Accepts the name of the Classroom from user
  # @return [String] Name of the Classroom
  def accept_name
    puts "\nEnter the name of Classroom :"
    name = gets.chomp
    return name if valid_classroom_name?(name)

    puts "\nInvalid Classroom Name"
    accept_name
  end

  # Accepts the number of students to be added in Classroom from user
  # @return [Integer] Number of students to be added in Classroom
  def accept_no_of_students
    puts "\nEnter the number of students to add in Classroom #{name} :"
    no_of_student = gets.chomp.to_i
    return no_of_student if valid_no_of_students?(no_of_student)

    puts "\nInvalid Input"
    accept_no_of_students
  end

  # Validates the name of Classroom
  # @param classroom_name [String] Name of the Classroom
  # @return [TrueClass, FalseClass]
  def valid_classroom_name?(classroom_name)
    classroom_name.match?(/^\w+(?: \w+)?$/)
  end

  # Validates the number of student
  # @param num_of_student [Integer] Number of students to be added in Classroom
  # @return [TrueClass, FalseClass]
  def valid_no_of_students?(num_of_student)
    num_of_student.positive?
  end

  # Sort the list of students by selected order
  # @param students [Array<Student>] list of students in Classroom
  # @return [Array<Student>] sorted list of students
  def sort_students_list(students)
    puts "\nSort Students as per \n1. Name\n2. Birth Date\n3. Roll Number\n4. Percentage"
    case gets.chomp.to_i
    when 1 then sort_students_by_name(students)
    when 2 then sort_students_by_birth_date(students)
    when 3 then sort_students_by_roll_no(students)
    when 4 then sort_students_by_percentage(students)
    else
      puts "\nInvalid Input. Please retry"
      sort_students_list(students)
    end
  end

  # Sort the list of students by the name
  # @param students [Array<Student>] list of students in Classroom
  # @return [Array<Student>] sorted list of students
  def sort_students_by_name(students)
    puts "\nStudents list(Sorted as per Name)"
    students.sort_by(&:name)
  end

  # Sort the list of students by the birthdate
  # @param students [Array<Student>] list of students in Classroom
  # @return [Array<Student>] sorted list of students
  def sort_students_by_birth_date(students)
    puts "\nStudents list(Sorted as per Birth Date)"
    students.sort_by(&:birth_date)
  end

  # Sort the list of students by the roll no
  # @param students [Array<Student>] list of students in Classroom
  # @return [Array<Student>] sorted list of students
  def sort_students_by_roll_no(students)
    puts "\nStudents list(Sorted as per Roll Number)"
    students.sort_by(&:roll_no)
  end

  # Sort the list of students by the percentage
  # @param students [Array<Student>] list of students in Classroom
  # @return [Array<Student>] sorted list of students
  def sort_students_by_percentage(students)
    puts "\nStudents list(Sorted as per Percentage)"
    students.sort_by(&:percentage)
  end
end
