require '../input'
# Class to manage the collection of integers
class Collection
  include Input
  # @return [Array<Integer>] Collection of integers
  attr_reader :array

  # @return [Integer] Value of the sum for which triplet is to be find in array
  attr_reader :sum

  # Default Constructor for class collection
  # @return [void]
  def initialize
    @array = []
  end

  # Accept size ,elements of array and value for sum from user
  # @return [void]
  def accept_input
    @array = Input.accept_array_elements
    @sum = accept_sum
  end

  # Prints triplet in array with sum equal to sum entered by user
  # @return [void]
  def print_triplet
    @array.sort!
    if (triplet = search_triplet).empty?
      puts "There is no Triplet in array whose sum is equal to #{sum}"
    else
      puts "The Triplet in array whose sum is equal to #{sum} :"
      puts triplet.to_s
    end
  end

  private

  # Accepts the value for the sum from user
  # @return [Integer]
  def accept_sum
    puts "\nEnter the value of sum :"
    sum = gets.chomp
    return sum.to_i if valid_sum?(sum)

    puts "\nInvalid Input"
    accept_sum
  end

  # Checks if the sum is valid Integer or not
  # @param sum [Integer] Value of the sum for which triplet is to be find in array
  # @return [TrueClass,FalseClass]
  def valid_sum?(sum)
    sum.match?(/^(?:-)?\d+?$/)
  end

  # Finds triplet in array with sum equal to sum entered by user
  # @return [Array<Integer>]
  def search_triplet
    result = []
    (0...array.length - 1).each do |index|
      sum_value = sum - array[index]
      left = index + 1
      right = array.length - 1
      while left < right
        result.insert(0, array[index], array[left], array[right]) if (array[left] + array[right]) == sum_value
        break unless result.empty?

        array[left] + array[right] > sum_value ? right -= 1 : left += 1
      end
      break unless result.empty?
    end
    result
  end
end

numbers = Collection.new
numbers.accept_input
puts "Input Array :\n#{numbers.array}"
numbers.print_triplet
