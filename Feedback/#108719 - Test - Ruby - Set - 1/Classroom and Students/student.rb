# #108719 - Test - Ruby - Set - 2 - Q.2 Classroom & Students
# Run school.rb to execute Q.2 Classroom & Students
require 'date'
# Class to manage Student information
class Student
  # @return [String] Name of the student
  attr_reader :name
  # @return [Integer] Roll no of the studunt
  attr_reader :roll_no
  # @return [Date] Birthdate of the student
  attr_reader :birth_date
  # @return [Float] Percentage of the studunt
  attr_reader :percentage
  # @return [String] Name of the classroom of student
  attr_reader :class_name

  # Stores all the instance of Student Class
  @@students = []

  # Find the list of students by the name of classroom
  # @param class_name [String] Name of the classroom of student
  # @return [Array<Student>] list of students
  def self.find_students_by_classroom(class_name)
    @@students.select { |student| student.class_name == class_name }
  end

  # Accepts the Student Information from user
  # @param classroom_name [String] Name of the classroom of student
  # @return [void]
  def accept_details(classroom_name)
    @roll_no = accept_rollno(classroom_name)
    @name = accept_name
    @birth_date = accept_birth_date
    @percentage = accept_percentage
    @class_name = classroom_name
    @@students << self
  end

  private

  # Validates the roll no of student
  # @param roll_no [String] Roll No of student
  # @return [TrueClass, FalseClass]
  def valid_rollno?(roll_no)
    roll_no.match?(/^\d+?$/) && roll_no.to_i.positive?
  end

  # Checks whether the roll no already exist in given classroom
  # @param rollno [Integer] Roll No of student
  # @param class_name [String] Name of the classroom of student
  # @return [TrueClass, FalseClass]
  def rollno_exist?(rollno, class_name)
    !!@@students.select { |student| student.roll_no == rollno && student.class_name == class_name }.first
  end

  # Validates the name of student
  # @param name [String] Name of student
  # @return [TrueClass, FalseClass]
  def valid_name?(name)
    name.match(/^[a-zA-Z]+(?: [a-zA-Z]+)?$/)
  end

  # Validates the birth date of student
  # @param date [Date] Birthdate of student
  # @return [TrueClass, FalseClass]
  def valid_date?(date)
    Date.parse(date).strftime('%-d %B %Y') == date
  end

  # Validates the percentage of student
  # @param percentage [String] Percentage of student
  # @return [TrueClass, FalseClass]
  def valid_percentage?(percentage)
    percentage.match?(/^\d+(\.\d+)?$/) && percentage.to_f.between?(0, 100)
  end

  # Accepts the Roll No of student from user
  # @param classroom_name [String] Name of the classroom of student
  # @param msg [String] Message to be displayed when method is invoked
  # @return [Integer]
  def accept_rollno(classroom_name, msg = 'Enter Roll Number :')
    puts msg
    rollno = gets.chomp
    return rollno.to_i if valid_rollno?(rollno) && !rollno_exist?(rollno.to_i, classroom_name)

    accept_rollno(classroom_name, "\nInvalid Roll Number")
  end

  # Accepts the Name of student from user
  # @return [String]
  def accept_name
    puts 'Enter Name :'
    name = gets.chomp
    return name if valid_name?(name)

    puts "\nInvalid Student Name"
    accept_name
  end

  # Accepts the birth date of student from user
  # @return [Date]
  def accept_birth_date
    puts 'Enter Birth Date : (Format : Date Name of Month Year Eg: 1 Janauary 1990)'
    date = gets.chomp
    return Date.parse(date) if valid_date?(date)

    puts "\nInvalid Birth Date"
    accept_birth_date
  end

  # Accepts the Percentage of student from user
  # @return [Float]
  def accept_percentage
    puts 'Enter Percentage :'
    percentage = gets.chomp
    return percentage.to_f if valid_percentage?(percentage)

    puts "\nInvalid Percentage"
    accept_percentage
  end
end
