# #108719 - Test - Ruby - Set - 2 - Q.2 Classroom & Students
require './classroom'
# Class to manage School information
class School
  # Manages various operations of school
  # @return [void]
  def self.manage_school
    loop do
      display_options
      case gets.chomp.to_i
      when 1 then Classroom.new.add_classroom
      when 2 then add_students_in_classroom
      when 3 then list_students_in_classroom
      when 4 then break
      else puts 'Invalid Input'
      end
    end
  end

  # Displays various options to manage school
  # @return [void]
  def self.display_options
    puts "\nChoose an options"
    puts "1. Create Classroom \n2. Add Students in Classroom\n3. List Students in Classroom\n4. Quit\n"
  end

  # Add Students in Classroom if the entered classroom exist
  # @return [void]
  def self.add_students_in_classroom
    puts "\nProvide the name of Classroom of students :"
    classroom = Classroom.find_classroom(gets.chomp)
    return puts "\nInvalid Classroom Name" if classroom.nil?

    classroom.add_student
  end

  # Displays list of students in Classroom if the entered classroom exist
  # @return [void]
  def self.list_students_in_classroom
    puts "\nEnter the name of Classroom :"
    classroom = Classroom.find_classroom(gets.chomp)
    return puts "\nInvalid Classroom Name" if classroom.nil?

    classroom.display_student_list
  end
end

School.manage_school
