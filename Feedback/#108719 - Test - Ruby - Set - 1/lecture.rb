# #108719 - Test - Ruby - Set - 1 - Q.5 Frustrated Professor
class Lecture
  attr_reader :total_students, :cancel_threshold, :delayed_minutes

  # Default Constructor for class Lecture
  def initialize
    @total_students = nil
    @cancel_threshold = nil
    @delayed_minutes = []
  end

  # Method to display status of lecture for each scenario
  def display_cancelled_status
    results = []
    accept_no_of_scenarios.times do |scenario_no|
      puts "\nEnter details for Scenario #{scenario_no + 1} :"
      accept_details
      accept_delayed_minutes
      results << lecture_cancelled
    end
    puts "\nLecture cancelled Status :"
    puts results.join("\n")
  end

  private

  # Method to accept the number of scenarios to test from  user
  def accept_no_of_scenarios
    puts 'Enter the no scenarios to check :'
    no_of_scenarios = gets.chomp.to_i
    return no_of_scenarios if validate_no_of_scenarios?(no_of_scenarios)

    puts "\nInvalid Input"
    accept_no_of_scenarios
  end

  # Method to validate the number of scenarios
  def validate_no_of_scenarios?(no_of_scenarios)
    no_of_scenarios.positive?
  end

  # Method to accept total number of students and cancel threshold from user
  def accept_details
    puts "\nEnter the total no students and minimum no of student required to start the lecture (Separated by Space) :"
    lecture_details = gets.strip.split
    if lecture_details.size == 2
      @total_students = lecture_details[0].to_i if validate_total_students?(lecture_details[0].to_i)
      @cancel_threshold = lecture_details[1].to_i if validate_cancel_threshold?(lecture_details[1].to_i)
      accept_details if total_students.nil? || cancel_threshold.nil?
    else
      accept_details
    end
  end

  # Method to validate total number of students
  def validate_total_students?(no_of_students)
    no_of_students.positive?
  end

  # Method to validate cancel threshold
  def validate_cancel_threshold?(threshold)
    threshold.positive? && threshold <= total_students
  end

  # Method to accept delay minutes for each student from user
  def accept_delayed_minutes
    puts 'Enter the delayed minutes for each student (Separated by Space) :'
    @delayed_minutes = gets.strip.split
    @delayed_minutes.map! { |delay_minute| Integer(delay_minute) }
    raise ArgumentError unless delayed_minutes.size == total_students
  rescue ArgumentError
    puts "\nInvalid Input"
    retry
  end

  # Method to count number of ontime students
  def count_ontime_students
    delayed_minutes.select { |delay_minute| delay_minute <= 0 }.size
  end

  # Method to check if lecture is cancelled or not
  def lecture_cancelled
    count_ontime_students < cancel_threshold ? 'Yes' : 'No'
  end
end
discrete_maths = Lecture.new
discrete_maths.display_cancelled_status
